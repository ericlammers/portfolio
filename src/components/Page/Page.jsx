import React, { useState, useEffect } from 'react';
import ToggleableInfoBar from './components/TogglableInfoBar/TogglableInfoBar';
import InfoBar, { minToDisplay } from "./components/InfoBar/InfoBar";
import MainContent from "./components/MainContent/MainContent";
import {createId} from "../../services/strings";
import {debounce, throttle} from "../../services/throttleAndDebounce";
import './Page.scss';

const Page = ({ content, sectionNames, startingLocation }) => {
    const [currentSection, setCurrentSection] = useState(sectionNames[0]);
    const [manuallyScrolling, setManuallyScrolling] = useState(true);

    let sectionIdsToNamesMap = {};
    sectionNames.forEach((sectionName) => {
        sectionIdsToNamesMap = {...sectionIdsToNamesMap, [createId(sectionName)]: sectionName };
    });

    const isAlreadyCurrentSection = sectionId => sectionId === createId(currentSection);

    const isHighInThePage = (yLocation) => {
        const windowHeight = window.innerHeight;
        return yLocation < (2 * windowHeight)/5;
    };

    const determineCurrentSection = () => {
        const sectionIds = sectionNames.map(sectionName => createId(sectionName));
        let candidateCurrentSection = sectionIds[0];

        for(let i = 1; i < sectionIds.length; i++) {
            const sectionId = sectionIds[i];
            const top = document.getElementById(sectionId).getBoundingClientRect().top;
            if(isHighInThePage(top)) {
                candidateCurrentSection = sectionId;
            }
        }

        if(!isAlreadyCurrentSection(candidateCurrentSection)) {
            setCurrentSection(sectionIdsToNamesMap[candidateCurrentSection]);
        }
    };

    // Called incrementally
    const determineCurrentSectionThrottled = throttle(() => {
        if(manuallyScrolling) {
            determineCurrentSection();
        }
    }, 100);

    // Called once scrolling ends
    const determineCurrentSectionDebounced = debounce(() => {
        if(!manuallyScrolling) {
            setManuallyScrolling(true);
        } else {
            determineCurrentSection();
        }
    }, 50);

    const scrollToSection = (sectionName) => {
        setManuallyScrolling(false);
        setCurrentSection(sectionName);
        document.getElementById(createId(sectionName)).scrollIntoView({
            behavior: 'smooth'
        });
    };

    useEffect(() => {
        window.addEventListener('scroll', determineCurrentSectionThrottled);
        window.addEventListener('scroll', determineCurrentSectionDebounced);

        return function cleanup() {
            window.removeEventListener('scroll', determineCurrentSectionThrottled);
            window.removeEventListener('scroll', determineCurrentSectionDebounced);
        };
    });

    useEffect(() => {
        if (startingLocation) {
            scrollToSection(startingLocation.replace('-', ' '))
        }
    }, [startingLocation]);

    return (
         <div className="page">
            <ToggleableInfoBar>
                <InfoBar
                    sectionNames={sectionNames}
                    currentSection={currentSection}
                    scrollToSection={scrollToSection}
                />
            </ToggleableInfoBar>
            <InfoBar
                sectionNames={sectionNames}
                currentSection={currentSection}
                scrollToSection={scrollToSection}
                minToDisplay={minToDisplay.medium}
            />
            <MainContent>
                {content}
            </MainContent>
        </div>
    );
};

export default Page;
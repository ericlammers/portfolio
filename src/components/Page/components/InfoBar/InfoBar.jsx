import React from 'react';
import {Link} from "react-router-dom";
import './InfoBar.scss'

export const minToDisplay = {
    medium: 'medium-and-larger',
};

const InfoBar = (
    { 
        minToDisplay, 
        sectionNames, 
        currentSection, 
        scrollToSection, 
        closeInfoBar
    }
) =>  {
    const sectionButtons = sectionNames.map((sectionName) => {
        const sectionClicked = () => {
            scrollToSection(sectionName);
            if (closeInfoBar) {
                closeInfoBar();
            }
        };

        if (sectionName.toLowerCase() === currentSection.toLowerCase()) {
            return (
                <button
                    key={sectionName}
                    onClick={sectionClicked}
                    style={{fontWeight: "bold", borderBottom: "2px solid"}}>
                    {sectionName}
                </button>
            )
        }

        return (
            <button 
                onClick={sectionClicked} 
                key={sectionName}
            >
                {sectionName}
            </button>
        );
    });

    return (
        <div className={`info-bar ${minToDisplay ? minToDisplay : ''}`}>
            <Link to="/">
                <img src={require('../../../../images/head-shot.png')} alt=""/>
            </Link>
            <div className="section-buttons">
                {sectionButtons}
            </div>
        </div>
    );
};


export default InfoBar;
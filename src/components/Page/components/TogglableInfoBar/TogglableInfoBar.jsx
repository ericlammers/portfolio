import React, { useState } from 'react';
import InfoButton from "./components/InfoButton/InfoButton";
import ClosableInfoBar from "./components/ClosableInfoBar/ClosableInfoBar";
import './TogglableInfoBar.scss'

const TogglableInfoBar = ({children}) => {
    const [opened, setOpened] = useState(false);

    const openInfoBar = () => setOpened(true);

    const closeInfoBar = () => setOpened(false);

    const component = opened ?
        <ClosableInfoBar closeInfoBar={closeInfoBar}>{children}</ClosableInfoBar> :
        <InfoButton openInfoBar={openInfoBar}/>;

    return (
        <div className="togglable-info-bar">
            {component}
        </div>
    );
};

export default TogglableInfoBar;
import React, {useEffect} from 'react';
import './ClosableInfoBar.scss'

const CloseableInfoBar = ({closeInfoBar, children}) =>  {
    useEffect(() => {
        window.addEventListener('scroll', closeInfoBar);

        return () => {
            window.removeEventListener('scroll', closeInfoBar);
        };
    });

    return (
        <div className="closable-info-bar">
            <span className="close-button" onClick={closeInfoBar}>
                <i className="fa fa-times" />
            </span>
            {React.Children.map(children, (child, i) => {
                return React.cloneElement(child, {closeInfoBar: closeInfoBar})
            })}
        </div>
    );
};


export default CloseableInfoBar;
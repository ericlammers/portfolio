import React from 'react';
import './InfoButton.scss'

const InfoButton = ({openInfoBar}) => (
    <div className="info-button" onClick={openInfoBar}>
        <i className="fa fa-bars" aria-hidden="true" />
    </div>
);

export default InfoButton;
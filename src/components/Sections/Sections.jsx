import React from 'react';
import {createId} from "../../services/strings";
import "./Sections.scss";

const Section = ({name, id, children}) => (
    <div id={id} className="section">
        <h2>{name}</h2>
        {children}
    </div>
);

const Sections = ({sectionInfoList}) => (
    <div className="content">
        {sectionInfoList.map(sectionInfo => (
            <Section key={sectionInfo.title} name={sectionInfo.title} id={createId(sectionInfo.title)}>
                {sectionInfo.component}
            </Section>
        ))}
    </div>
);

export default Sections;
import React from 'react';
import './List.scss';

const List = ({ items, description }) => (
    <div className="items">
        <div>
            {description ? description.map(item => <div><div>{item}</div><br/></div>) : ''}
        </div>
        <ul>
            {items.map(item => (
                <li>
                    <a href={item.link} rel="noopener noreferrer" target="_blank">
                        {item.name}
                    </a>
                </li>
            ))}
        </ul>
    </div>
);

export default List;
export const COLOR = {
    red: "#ec5453",
    purple: "#a84cb8",
    yellow: "#f9bf3f",
    blue: "#2c98f0",
    green: "#1DA598",
    orange: "#FF8C00",
};
export const createId = (string) => string.replace(/\s+/g, '-').toLowerCase();

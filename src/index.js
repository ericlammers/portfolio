import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter } from 'react-router-dom'
import Layout from './Layout';
import * as serviceWorker from './serviceWorker';
import './polyfills';
import './index.css';

ReactDOM.render(
    <HashRouter basename={process.env.PUBLIC_URL}>
        <Layout />
    </HashRouter>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

import React from 'react';
import HomePage from "./scenes/Home/HomePage";
import 'typeface-quicksand';
import './Layout.scss';
import './variables.scss'
import Projects from "./scenes/Projects/Projects";
import { Route, Switch } from "react-router-dom";
import StayingCurrent from "./scenes/StayingCurrent/StayingCurrent";

function Layout() {
  return (
    <div className="layout">
        <Switch>
            <Route exact path='/' component={HomePage}/>
            <Route path='/projects' component={Projects}/>
            <Route path='/staying-current' component={StayingCurrent}/>
        </Switch>
    </div>
  );
}

export default Layout;

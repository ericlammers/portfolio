import React from 'react';
import './SlantedDiv.scss';

const SlantedDiv = ({ children, backgroundColor }) => (
    <div className="slanted-div">
        <div className='top' style={{ backgroundColor }}></div>
        <div className="middle" style={{ backgroundColor }}>
            {children}
        </div>
        <div className='bottom' style={{ backgroundColor }}></div>
    </div>
);

export default SlantedDiv;

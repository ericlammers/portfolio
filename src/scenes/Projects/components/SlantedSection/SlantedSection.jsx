import React from 'react';
import './SlantedSection.scss';
import SlantedDiv from './components/SlantedDiv/SlantedDiv';
import { createId } from '../../../../services/strings';

const SlantedSection = ({ header, items }) => {
  const componentList = [];

  items.forEach((item, index) => {
    if (index % 2 === 0) {
      componentList.push(
        <div className="regular-div">{item}</div>
      );
    } else {
      componentList.push(
        <SlantedDiv backgroundColor="#eee">{item}</SlantedDiv>

      );
    }
  });

  return (
    <div id={createId(header.title)} className="slanted-section">
      <SlantedDiv backgroundColor={header.color}>
        <div className="header">{header.title}</div>
      </SlantedDiv>
      {componentList}
    </div>
  );
}

export default SlantedSection;
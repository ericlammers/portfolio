import React from 'react';
import './Technologies.scss';

const Technologies = ({ technologies }) => (
    <div className="technologies">
        <ul>
            {technologies.map(technology => (
                <li>
                    <a href={technology.link} rel="noopener noreferrer" target="_blank">
                        {technology.name}
                    </a>
                </li>
            ))}
        </ul>
    </div>
);

export default Technologies;
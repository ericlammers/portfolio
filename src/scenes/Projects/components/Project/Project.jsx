import React from 'react';
import Technologies from './components/Technologies/Technologies';
import uniqid from 'uniqid';
import './Project.scss';

const Project = ({ project }) => {
  const description = project.description.map(paragraph => <div key={uniqid()}>{paragraph}</div>);

  const getTakeaways = () => {
    if(project.takeaways && project.takeaways.length !== 0) {
      const takeaways = project.takeaways.map(takeaway => (
        <li key={uniqid()}><b>{takeaway.header}</b>{` ${takeaway.content}`}</li>
      ));

      return (
        <div className="section">
          <div className="sub-title">Takeaways:</div>
          <ul className="takeaways">{takeaways}</ul>
        </div>
      );
    } else {
      return null;
    }
  }

  const getSourceCode = () => {
    if(project.sourceCode) {
      return (
        <div className="section source-code">
          <div className="sub-title">Source Code:</div>
          <a
            className="source-code-link"
            rel="noopener noreferrer"
            target="_blank"
            href={project.sourceCode}>
            {project.sourceCode}
          </a>
        </div>
      );
    } else {
      return null;
    }
  }

  return (
    <div className="project">
      <h2 className="title" style={{ borderColor: project.color }}>{project.title}</h2>
      <div className="section">
        <div className="description">{description}</div>
      </div>
      {getTakeaways()}
      <div className="section">
        <div className="sub-title">Technologies:</div>
        <Technologies technologies={project.stack} />
      </div>
      {getSourceCode()}
    </div>
  );
}

export default Project;
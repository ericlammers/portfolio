import { COLOR } from '../../../services/colors';

const jobSiteManagerUI = {
    title: "Job Site Manager UI",
    color: COLOR.orange,
    description: [
        "The Job Site Manager App is a web application that helps companies who operate at multiple" 
        + " different job sites manage and track which employees are assigned to each site. The UI" 
        + " supports a range of functionality"
        + " including adding new employees, assigning employees designated roles, creating jobs sites and"
        + " adding/removing employees with the necessary qualifications to a job. ",
    ],
    takeaways: [
        {
            header: "I'm sold on react hooks.",
            content: "Using functional components with hooks is a cleaner and more consice solution then using class components."
        },
        {
            header: "Jury is still out on css-in-js.",
            content: "I like how styled components allow you to have all functionally" 
        + " related code in one place but I find it easier to organize my styling using scss.",
        },
        {
            header: "Redux adds a lot of overhead.",
            content: "Redux's is great at managing the state of complex applications but you shouldn't underestimate the amount of time it "
         + "takes to implement. Think critically about whether it is necessary before introducing it into your stack."
        }
        
    ],
    stack: [
        {
            name: "React",
            link: "https://reactjs.org/"
        },
        {
            name: "Redux",
            link: "https://redux.js.org/"
        },
        {
            name: "ES6",
            link: "http://es6-features.org/#Constants"
        },
        {
            name: "Styled Components",
            link: "https://www.styled-components.com/"
        },
        {
            name: "Jest",
            link: "https://jestjs.io/"
        },
        {
            name: "Flexbox",
            link: "https://css-tricks.com/snippets/css/a-guide-to-flexbox/"
        },
        {
            name: "Create React App",
            link: "https://github.com/facebook/create-react-app"
        },
    ],
    sourceCode: "https://gitlab.com/Job-Site-Manager/job-site-manager-ui"
}

const portfolio = {
    title: "My Portfolio",
    color: COLOR.blue,
    description: [
        "My portfolio is the web application you are viewing right now. It's a React app that I put together" 
        + " to give people some insight into who I am and the things I've been up to. The code was built using create-react-apps build " +
        " script and is hosted on github pages.",
    ],
    takeaways: [
        {
            header: "Reacts useEffect hook is money.",
            content: "useEffects replaces all lifecycle methods for a component and is simple and intuitive to use."
        },
        {
            header: "Github pages is an awesome resource.",
            content: "For personal sites like this one it's really nice to have a free option for hosting.",
        },
        {
            header: "Coding outdoors is hard to beat.",
            content: "Most of this app was written while sitting on a lawn chair in the shade. It's hard to balance getting enough fresh air "
            + " and coding in your free time so when you can combine the two it's alright."
        },
    ],
    stack: [
        {
            name: "React",
            link: "https://reactjs.org/"
        },
        {
            name: "ES6",
            link: "http://es6-features.org/#Constants"
        },
        {
            name: "Flexbox",
            link: "https://css-tricks.com/snippets/css/a-guide-to-flexbox/"
        },
        {
            name: "Create React App",
            link: "https://github.com/facebook/create-react-app"
        },
        {
            name: "React Router",
            link: "https://github.com/ReactTraining/react-router"
        },
        {
            name: "Sass",
            link: "https://sass-lang.com/"
        },
        {
            name: "Github Pages",
            link: "https://pages.github.com/"
        }
    ],
    sourceCode: "https://gitlab.com/ericlammers/portfolio"
}

const jobSiteManagerApi = {
    title: "Job Site Manager API",
    color: COLOR.purple,
    description: [
        "Job Site Manager App is a web application that helps companies who operate at multiple different job sites manage and track which employees are assigned to each site. The API is a Spring Boot web app which handles the manipulation, validation and storage of the job site data. This includes adding, editing and removing employees, employee roles and job sites.",
    ],
    takeaways: [],
    stack: [
        {
            name: "Spring Boot",
            link: "https://spring.io/projects/spring-boot"
        },
        {
            name: "JUnit",
            link: "https://junit.org/junit5/"
        },
        {
            name: "MySql",
            link: "https://www.mysql.com/"
        },
        {
            name: "MyBatis",
            link: "http://www.mybatis.org/mybatis-3/"
        },
        {
            name: "Docker",
            link: "hhttps://www.docker.com/"
        },
        {
            name: "Docker Compose",
            link: "https://docs.docker.com/compose/"
        },
    ],
    sourceCode: "https://gitlab.com/Job-Site-Manager/job-site-manger-api"
}

const tradeWatchAPIGatewayAndAPI = {
    title: "Trade Watch API Gateway and Register",
    color: COLOR.red,
    description: [
        "The main motivation for the trade watch project was to gain a better understanding of service oriented architectures. The Trade Watch API Gateway and Register is an express server that dynamically registers and deregisters microservices. It also handles the routing of requests to the microservices. "
    ],
    takeaways: [
        {
            header: "Docker is a great tool for local development.",
            content: "While developing the Gateway locally I needed to test the service with multiple local microservices. Using docker and docker-compose made it easy to spin up multiple instances of a single test microservice."
        },
    ],
    stack: [
        {
            name: "Express",
            link: "https://expressjs.com/"
        },
        {
            name: "Node",
            link: "https://nodejs.org/en/"
        },
        {
            name: "MongoDB",
            link: "https://www.mongodb.com/"
        },
        {
            name: "Mongoose",
            link: "https://mongoosejs.com/"
        },
        {
            name: "Axios",
            link: "https://github.com/axios/axios"
        },
        {
            name: "Express Http Proxy",
            link: "https://www.npmjs.com/package/express-http-proxy"
        },
        {
            name: "Docker",
            link: "hhttps://www.docker.com/"
        },
        {
            name: "Docker Compose",
            link: "https://docs.docker.com/compose/"
        },
    ],
    sourceCode: "https://gitlab.com/ericlammers/trade-watch-api-gateway-and-register"
}

const reactiveApplication = {
    title: "Reactive App",
    color: COLOR.green,
    description: [
        "A sample application I put together to get some experience with the new reactive features in Spring Boot 2.0. I also built a front end react app with real-time streaming to test the API. For local development I included a docker-compose file that spins up a mongodb container."
    ],
    stack: [
        {
            name: "Spring Boot",
            link: "https://spring.io/projects/spring-boot"
        },
        {
            name: "Spring WebFlux",
            link: "https://docs.spring.io/spring/docs/current/spring-framework-reference/web-reactive.html"
        },
        {
            name: "Spring Data Reactive MongoDB",
            link: "https://www.baeldung.com/spring-data-mongodb-reactive"
        },
        {
            name: "React",
            link: "https://reactjs.org/"
        },
        {
            name: "WebSocket",
            link: "https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API"
        },
        {
            name: "Docker",
            link: "hhttps://www.docker.com/"
        },
        {
            name: "Docker Compose",
            link: "https://docs.docker.com/compose/"
        },
    ],
    sourceCode: "https://gitlab.com/ericlammers/reactive-application"
}

const tradeWatchKubernetesCluster = {
    title: "Trade Watch Kubernetes Cluster",
    color: COLOR.orange,
    description: [
        "Created a project with all the configuration files required to set up a Kubernetes cluster for the Trade Watch application. Contains configuration files for a number of pods and services. Additionally I created docker containers for each of the microservices. The deployment was tested locally using mini kube and then deployed to IBM Cloud."
    ],
    stack: [
        {
            name: "Kubernetes",
            link: "https://kubernetes.io/"
        },
        {
            name: "Docker",
            link: "https://www.docker.com/"
        },
        {
            name: "IBM Cloud",
            link: "https://www.ibm.com/cloud"
        },
        {
            name: "Mini Kube",
            link: "https://github.com/kubernetes/minikube"
        },
    ],
    sourceCode: "https://gitlab.com/ericlammers/trade-watch-kubernetes-configurations"
}

const rockClimbingApp = {
    title: "Rock Climbing App",
    color: COLOR.purple,
    description: [
        "I’m doing some part time work for two brothers who have a rock climbing mobile app idea. We’ve chosen to use React Native, Redux, Expo and Typescript to develop the app. This is my first project using React Native and it's exciting to see how much of my React knowledge transfers over. This is also my first time using Typescript and I’m really enjoying it. It provides a lot of benefits over Javascript including easier to read code and a more product development environment."
    ],
    stack: [
        {
            name: "React Native",
            link: "https://facebook.github.io/react-native/"
        },
        {
            name: "Typescript",
            link: "https://www.typescriptlang.org/"
        },
        {
            name: "Node",
            link: "https://nodejs.org/en/"
        },
        {
            name: "Expo",
            link: "https://expo.io/"
        },
        {
            name: "Redux",
            link: "https://redux.js.org/"
        },
        {
            name: "Native Base",
            link: "https://nativebase.io/"
        },
        {
            name: "Jest",
            link: "https://jestjs.io/"
        },
    ],
}

const projects = {
    frontEnd: [ jobSiteManagerUI,  portfolio],
    backEnd: [ jobSiteManagerApi, tradeWatchAPIGatewayAndAPI, reactiveApplication ],
    devOps: [ tradeWatchKubernetesCluster ],
    mobile: [ rockClimbingApp ]
}

export default projects;
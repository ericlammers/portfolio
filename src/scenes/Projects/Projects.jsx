import React from 'react';
import Page from "../../components/Page/Page";
import SlantedSection from "./components/SlantedSection/SlantedSection";
import Project from './components/Project/Project';
import queryString from 'query-string';
import { COLOR } from '../../services/colors';
import projects from './data/projects';

const ProjectComponent = <Project project={projects.frontEnd[0]} />;

const Projects = ({ location }) => {
  const section = location && location.search && queryString.parse(location.search).section;

  const sectionNames = ["Front End", "Back End", "DevOps", "Mobile"];

  const content = (
    <div className="container">
      <SlantedSection
        header={{ color: COLOR.green, title: "Front End" }}
        items={projects.frontEnd.map(project => <Project project={project} />)}
      />
      <SlantedSection
        header={{ color: COLOR.orange, title: "Back End" }}
        items={projects.backEnd.map(project => <Project project={project} />)} 
      />
      <SlantedSection
        header={{ color: COLOR.blue, title: "DevOps" }}
        items={projects.devOps.map(project => <Project project={project} />)} 
      />
      <SlantedSection
        header={{ color: COLOR.yellow, title: "Mobile" }}
        items={projects.mobile.map(project => <Project project={project} />)}
      />
    </div>
  );

  return (
    <div>
      <Page
        content={content}
        startingLocation={section}
        sectionNames={sectionNames}
      />
    </div>
  );
};

export default Projects;
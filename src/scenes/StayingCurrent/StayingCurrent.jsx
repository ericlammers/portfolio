import React from 'react';
import Page from "../../components/Page/Page";
import Sections from "../../components/Sections/Sections";
import List from '../../components/List/List';

const podcasts = [
    {
        link: 'https://mastersofscale.com/',
        name: 'Masters of Scale'
    },
    {
        link: 'https://reactpodcast.simplecast.fm/',
        name: 'React Podcast'
    },
    {
        link: 'https://syntax.fm/',
        name: 'Syntax'
    },
    {
        link: 'https://www.codingblocks.net/',
        name: 'Coding Blocks'
    },
    {
        link: 'https://devchat.tv/js-jabber/',
        name: 'JavaScript Jabber '
    },
];

const textbooks = [
    {
        link: 'https://www.oreilly.com/library/view/software-architecture-in/9780132942799/',
        name: 'Software Architecture in Practise'
    },
    {
        link: 'https://www.oreilly.com/library/view/clean-code/9780136083238/',
        name: 'Clean Code'
    },
    {
        link: 'https://www.oreilly.com/library/view/the-art-of/9781617290893/',
        name: 'The Art of Unit Testing'
    },
    {
        link: 'https://www.fullstackreact.com/',
        name: 'Fullstack React: The Complete Guide to ReactJs and Friends'
    },
    {
        link: 'https://www.manning.com/books/spring-in-action-fourth-edition',
        name: 'Spring in Action, 4th Edition'
    },
];

const blogs = [
    {
        link: 'https://www.freecodecamp.org/news/learn-kubernetes-in-under-3-hours-a-detailed-guide-to-orchestrating-containers-114ff420e882/',
        name: 'Learn Kubernetes in Under 3 Hours: A Detailed Guide to Orchestrating Containers'
    },
    {
        link: 'https://developer.okta.com/blog/2018/09/24/reactive-apis-with-spring-webflux',
        name: 'Build Reactive APIs with Spring WebFlux'
    },
    {
        link: 'https://medium.com/@muneebsajjad/git-flow-explained-quick-and-simple-7a753313572f',
        name: 'Git Flow Explained: Quick and Simple'
    },
    {
        link: 'https://medium.com/@alexmngn/how-to-better-organize-your-react-applications-2fd3ea1920f1',
        name: 'How to better organize your React applications?'
    },
    {
        link: 'https://www.robertcooper.me/get-started-with-typescript-in-2019/',
        name: 'Get Started With Typescript in 2019'
    },
    {
        link: 'https://www.freecodecamp.org/news/graphql-zero-to-production-a7c4f786a57b/',
        name: 'How to build a full GraphQL server with Node.js'
    },
];

const Codepen = () => (
    <div>
        Codepen is a great place to try new things or throw together together initial versions of UI components. See my codepen account at
        <a style={{marginLeft: '3px'}} href="https://codepen.io/ericjameslammers/" rel="noopener noreferrer" target="_blank">
            https://codepen.io/ericjameslammers/
        </a>
        .
    </div>
);

const StayingCurrent = () => {
    const sectionInfoList = [
        {
            title: 'Podcasts',
            component: <List description={["Podcasts are a great way to stay up to date with the trends in technology.", "Some of my favourite podcasts include:"]} items={podcasts} />,
        },
        {
            title: 'Blogs and Posts',
            component: <List description={["Blogs and posts are a quick and effective way to get up to speed with new technologies.", "Some of the blogs I've found useful include:"]} items={blogs} />,
        },
        {
            title: 'Codepen',
            component: <Codepen />,
        },
        {
            title: 'Textbooks',
            component: <List description={["Textbooks are a great way to gain a deeper understanding of a technology.", "Some of the textbooks I've found useful include:"]} items={textbooks} />,
        },
    ];

    const content = (
        <Sections sectionInfoList={sectionInfoList} />
    );

    return (
        <div>
            <Page
                content={content}
                sectionNames={sectionInfoList.map(section => section.title)}
            />
        </div>
    );
};

export default StayingCurrent;
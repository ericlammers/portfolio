import React from 'react';
import './Volunteer.scss';

const Volunteer = () => (
    <div className="volunteer">
        <div className="header">
            <div className="title">Junior Badminton Head Coach</div>
            <div className="sub-title">March 2019 - May 2019</div>
        </div>
        Ran bi-weekly practices and coached at tournaments for a twenty-student badminton team.
    </div>
);

export default Volunteer;
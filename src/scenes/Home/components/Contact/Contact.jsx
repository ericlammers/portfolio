import React from 'react';
import './Contact.scss';

const Contact = () => (
    <div className="contact">
        <b>Email:</b> ericjameslammers@gmail.com
    </div>
);

export default Contact;
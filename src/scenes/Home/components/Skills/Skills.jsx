import React from 'react';
import Skill from "./components/Skill/Skill";
import { COLOR } from '../../../../services/colors';
import './Skills.scss';

const Skills = () => {
    const skills = [
        {
            color: COLOR.red,
            name: 'React',
            percentage: 90,
        },
        {
            color: COLOR.orange,
            name: 'Redux',
            percentage: 90,
        },
        {
            color: COLOR.yellow,
            name: 'JavaScript (ES6)',
            percentage: 90,
        },
        {
            color: COLOR.blue,
            name: 'HTML/CSS',
            percentage: 80,
        },
        {
            color: COLOR.green,
            name: 'SCSS',
            percentage: 80,
        },
        {
            color: COLOR.purple,
            name: 'Typescript',
            percentage: 65,
        },
        {
            color: COLOR.orange,
            name: 'Webpack',
            percentage: 70,
        },
        {
            color: COLOR.purple,
            name: "Docker",
            percentage: 80,
        },
        {
            color: COLOR.red,
            name: "Kubernetes",
            percentage: 70,
        },
        {
            color: COLOR.yellow,
            name: 'React Native',
            percentage: 60,
        },
        {
            color: COLOR.blue,
            name: 'Express',
            percentage: 70,
        },
        {
            color: COLOR.green,
            name: 'MongoDB',
            percentage: 60,
        },
        {
            color: COLOR.orange,
            name: 'Spring/Spring Boot',
            percentage: 80,
        },
        {
            color: COLOR.red,
            name: 'Java',
            percentage: 90,
        },
        {
            color: COLOR.purple,
            name: 'MySql/SQL',
            percentage: 75,
        },
        {
            color: COLOR.blue,
            name: 'Reactive Programming',
            percentage: 60,
        },
    ];

    return (
        <div className="skill-list">
            {skills.map(skill => <Skill {...skill} />)}
        </div>
    )
};

export default Skills;
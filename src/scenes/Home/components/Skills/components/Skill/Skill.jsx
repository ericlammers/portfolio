import React from 'react';
import ProgressBar from "./components/ProgressBar/ProgressBar";
import './Skill.scss'

const Skill = ({name, percentage, color}) => (
    <div className="skill">
        <div className="name">
            {name}
        </div>
        <ProgressBar percentage={percentage} color={color} />
    </div>
);

export default Skill;
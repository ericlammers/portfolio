import React from 'react';
import './ProgressBar.scss';
import Filler from "../Filler/Filler";

const ProgressBar = ({percentage, color}) => {
    const textLocation = percentage < 90 ? `${percentage + 2}%` : '92%';
    const fillerPercentage = `${percentage}%`;

    return (
        <div className="progress-bar">
            <div className="text">
          <span style={{marginLeft: textLocation, color: color}}>
            {fillerPercentage}
          </span>
            </div>
            <div className="display">
                <Filler percentage={fillerPercentage} color={color} />
            </div>
        </div>
    );
};

export default ProgressBar;
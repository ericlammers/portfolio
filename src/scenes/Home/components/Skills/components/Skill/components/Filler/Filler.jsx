import React from 'react';

const Filler = ({percentage, color}) => (
    <div className="filler" style={{ width: percentage, backgroundColor: color}} />
);

export default Filler;
import React from 'react';
import './ProjectCard.scss';

const ProjectCard = ({name, iconName, color}) => {
    const style = {
        color: color,
        borderColor: color
    };

    return (
        <div className="project-card" style={style}>
            <div className="icon">
                <i className={iconName} />
            </div>
            <div className="card-title">
                {name}
            </div>
        </div>
    );
};

export default ProjectCard;
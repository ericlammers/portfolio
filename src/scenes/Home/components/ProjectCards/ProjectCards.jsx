import React from 'react';
import './ProjectCards.scss';
import ProjectCard from "./components/ProjectCard/ProjectCard";
import { Link } from "react-router-dom";
import { COLOR } from '../../../../services/colors';

const ProjectCards = () => (
    <div className="project-cards">
        <Link to="/projects?section=front-end">
            <ProjectCard name="Front End Development" iconName="fa fa-code" color={COLOR.green} />
        </Link>
        <Link to="/projects?section=back-end">
            <ProjectCard name="Back End Development" iconName="fa fa-database" color={COLOR.orange} />
        </Link>
        <Link to="/projects?section=devops">
            <ProjectCard name="DevOps" iconName="fa fa-cloud-upload" color={COLOR.blue} />
        </Link>
        <Link to="/projects?section=mobile">
            <ProjectCard name="Mobile Development" iconName="fa fa-cloud-upload" color={COLOR.yellow} />
        </Link>
        <Link to="staying-current">
            <ProjectCard name="Staying Current" iconName="fa fa-book" color={COLOR.red} />
        </Link>
    </div>
);

export default ProjectCards;
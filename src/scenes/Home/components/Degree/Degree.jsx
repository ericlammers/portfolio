import React from 'react';
import './Degree.scss';

const Education = () => (
    <div className="degree">
        <div className="header">
            <div className="details">
                <div className="title">Specialization in Computer Science</div>
                <div className="sub-title">Bachelor of Science</div>
                <div className="school sub-title">University of Western Ontario</div>
            </div>
            <div className="date-range">September 2016 – April 2019</div>
        </div>
        <div className="achievements">
            <div>Graduated with Distinction</div>
            <div>Dean's Honor List</div>
        </div>
    </div>
);

export default Education;
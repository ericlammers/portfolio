import React from 'react';
import TimelineItem from "./components/TimelineItem/TimelineItem";
import JobDescription from "./components/JobDescription/JobDescription";
import { COLOR } from '../../../../services/colors';
import './Timeline.scss';

const JobTimeline = () => {
    // TODO Job Descriptions should include full or part time work
    const timelineJobs = [
        {
            color: COLOR.orange,
            job: {
                title: 'Software Developer',
                employer: 'Autodata Solutions',
                dateRange: 'May 2019 - Present',
                description: 'I do full stack web development with a focus on the front end React applications. Our React stack includes Redux for state management, SCSS for styling and Jest for unit testing. I am also involved in some of the backend development where we leverage the Spring Java framework to build restful APIs.'
            }
        },
        {
            color: COLOR.blue,
            job: {
                title: 'Web Developer Intern',
                employer: 'Autodata Solutions',
                dateRange: 'May 2017 - August 2018',
                description: 'As an intern at Autodata Solutions I worked directly with the technical lead to determine the ecosystem our company would use for React development. I was also actively involved in the development of the companies first React application. During the project I oversaw the work of the projects offshore team and was responsible for assigning them work and reviewing their code.'
            }
        },
        {
            color: COLOR.red,
            job: {
                title: 'Fullstack Developer',
                employer: 'Western University',
                dateRange: 'September 2016 - April 2017',
                description: 'Worked part-time as a web developer for a professor at the university. I built an Express/Jquery App that could be used to input data into an existing mobile apps mongodb database.'
            }
        },
    ];

    const Jobs = timelineJobs.map(timelineJob => (
        <TimelineItem color={timelineJob.color}>
            <JobDescription {...timelineJob.job} />
        </TimelineItem>
    ));

    return (
        <div className="job-timeline">
            {Jobs}
        </div>
    )
};

export default JobTimeline;
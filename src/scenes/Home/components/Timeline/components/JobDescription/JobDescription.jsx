import React from 'react';
import './JobDescription.scss';

const JobDescription = ({title, employer, dateRange, description}) => (
    <div className="job-description">
        <div className="header">
            <div className="title">{title}</div>
            <div className="sub-title date-range">{dateRange}</div>
            <div className="sub-title employer">{employer}</div>
        </div>
        {description}
    </div>
);

export default JobDescription;
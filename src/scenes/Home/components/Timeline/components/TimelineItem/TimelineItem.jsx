import React from 'react';
import TimelineBar from "./components/TimelineBar/TimelineBar";
import './TimelineItem.scss';

const TimelineItem = ({color, children}) => (
    <div className="timeline-item">
        <TimelineBar color={color} />
        <div className="content">
            {children}
        </div>
    </div>
);

export default TimelineItem;
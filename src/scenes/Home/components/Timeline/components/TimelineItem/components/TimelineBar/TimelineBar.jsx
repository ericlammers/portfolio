import React from 'react';
import Circle from "../Circle/Circle";
import './TimelineBar.scss';

const TimelineBar = ({color}) => (
    <div className="timeline-bar">
        <div className="bar"/>
        <div className="circle-container">
            <Circle color={color}/>
        </div>
    </div>
);

export default TimelineBar;
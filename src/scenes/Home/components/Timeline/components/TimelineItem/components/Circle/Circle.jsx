import React from 'react';
import './Circle.scss'

const Circle = ({color}) => (
    <div
        className="circle"
        style={{backgroundColor: color}}
    >
    </div>
);

export default Circle;
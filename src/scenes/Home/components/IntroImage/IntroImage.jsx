import React from 'react';
import './IntroImage.scss';

const IntroImage = () => (
    <div className="intro-image">
        <div className="our-picture">
        </div>
        <span className="text"> Fullstack<br />developer.<br />React<br />enthusiast.</span>
    </div>
);

export default IntroImage;
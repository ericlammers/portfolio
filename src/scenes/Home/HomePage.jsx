import React from 'react';
import Page from "../../components/Page/Page";
import Sections from "../../components/Sections/Sections";
import JobTimeline from "./components/Timeline/JobTimeline";
import Skills from "./components/Skills/Skills";
import ProjectCards from "./components/ProjectCards/ProjectCards";
import IntroImage from "./components/IntroImage/IntroImage";
import Degree from './components/Degree/Degree';
import Volunteer from './components/Volunteer/Volunteer';
import Contact from './components/Contact/Contact';

const HomePage = () => {
    const sectionInfoList = [
        {
            title: 'About',
            component: <ProjectCards />,
        },
        {
            title: 'Skills',
            component: <Skills/>,
        },
        {
            title: 'Work Experience',
            component:  <JobTimeline />,
        },
        {
            title: 'Degree',
            component: <Degree />,
        },
        {
            title: 'Volunteering',
            component: <Volunteer />,
        },
        {
            title: 'Contact',
            component: <Contact />,
        },
    ];
    const content = (
        <div>
            <IntroImage />
            <Sections sectionInfoList={sectionInfoList}/>
        </div>
    );

    return (
        <div>
            <Page 
                content={content} 
                sectionNames={sectionInfoList.map(sectionInfo => sectionInfo.title)}
            />
        </div>
    );
};

export default HomePage;